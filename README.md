# advanced-css-sass-natours

This is a project I built using advanced CSS and SASS that shows how to build webpages using Flow Layout and how to manage projects in accordance to best practices and known methodologies. There are also a lot of amazing CSS techniques applied to this modern webpage. Credit to Jonas Schmedtmann, author of an online course, for the knowledge imparted. For more amazing CSS techniques, visit http://codingheroes.io/

I used Node Package Manager(NPM) to manage this project including the compilation of SASS.

Visit the webpage on the following link:
https://natours-project-davidericjohn.firebaseapp.com/
